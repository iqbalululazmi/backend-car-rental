const express = require('express');

const UsersController = require('../controllers/api/UsersController');
const AuthController = require('../controllers/api/AuthController');
const ProfileController = require('../controllers/api/ProfileController');

function routes() {
    const router = express.Router();

    router.get('/user', function (req, res) {
        UsersController.index(req, res);
    });

    router.post('/user', function (req, res) {
        UsersController.create(req, res);
    });

    router.put('/user/:userID', function (req, res) {
        UsersController.update(req, res);
    });

    router.delete('/user/:userID', function (req, res) {
        UsersController.remove(req, res);
    });

    router.get('/user/:userID', function (req, res) {
        UsersController.getByUserID(req, res);
    });

    router.post('/auth/login', function (req, res) {
        AuthController.login(req, res);
    });

    router.get('/profile', function (req, res) {
        ProfileController.getProfile(req, res);
    });

    router.get('/testing', function (req, res) {
        res.status(200).json({
            message: 'Hello Heroku!',
        });
    });

    return router;
}

module.exports = routes;
